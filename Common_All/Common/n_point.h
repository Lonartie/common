#ifndef COMMON_H
#define COMMON_H

#include <memory>

namespace common
{
    namespace maths
    {

        // generic template
        template<int N, typename T>
        class N_Point : public N_Point<N - 1, T>
        {
        public:

            N_Point(T component);

            N_Point(T& component);

            N_Point(T&& component);

            N_Point();

            T GetComponentCopy() const;

            T& GetComponent();

            const T& GetComponentSafe() const;

        private:

            T component;
            T& ref_component;
        };
    }
}




// specialized base template
template<typename T>
class common::maths::N_Point<1, T>
{
public:

    N_Point(T component)
        :   component(std::move(component)),
            ref_component(this->component)
    {}

    N_Point(T& component)
        :   ref_component(component)
    {}

    N_Point(T&& component)
        :   component(std::move(component)),
            ref_component(this->component)
    {}

    N_Point()
        :   component(T{}),
            ref_component(this->component)
    {}

    T GetComponentCopy() const
    {
        return ref_component;
    }

    T& GetComponent()
    {
        return ref_component;
    }

    const T& GetComponentSafe() const
    {
        return ref_component;
    }

private:

    T component;
    T& ref_component;
};

#endif // COMMON_H
