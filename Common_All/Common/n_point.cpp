#include "n_point.h"

namespace common
{
    namespace maths
    {
        template<int N, typename T>
        N_Point<N, T>::N_Point(T component)
            :   component(std::move(component)),
                ref_component(this->component)
        {}

        template<int N, typename T>
        N_Point<N, T>::N_Point(T &component)
            :   ref_component(component)
        {}

        template<int N, typename T>
        N_Point<N, T>::N_Point(T &&component)
            :   component(std::move(component)),
                ref_component(this->component)
        {}

        template<int N, typename T>
        N_Point<N, T>::N_Point()
            :   component(T{}),
                ref_component(this->component)
        {}

        template<int N, typename T>
        T N_Point<N, T>::GetComponentCopy() const
        {
            return ref_component;
        }

        template<int N, typename T>
        T &N_Point<N, T>::GetComponent()
        {
            return ref_component;
        }

        template<int N, typename T>
        const T &N_Point<N, T>::GetComponentSafe() const
        {
            return  ref_component;
        }
    }
}
