#include "ltest_starter.h"
#include "ltest.h"

#include <QString>
#include <QStringList>
#include <QDebug>

#include <iostream>

int TestStarter::RunTestsConsole(QString tests)
{
    QStringList list = tests.split(";");
    std::cout << "tests: " << (tests.isEmpty() ? "all" : tests.toStdString()) << std::endl;
    return 0;
}

int TestStarter::RunTestsWindow(QString tests)
{
    qDebug() << "tests: " << tests;
    int n = 0;
    QApplication a(n, {});
    ltest_widget widget;
    widget.show();
    return a.exec();
}

int TestStarter::ListAllTests()
{
    for (auto test : LTest::All_Tests)
        std::cout << QString("%1/%2")
            .arg(test.name_space)
            .arg(test.name).toStdString() << std::endl;
    return 0;
}

int TestStarter::init(int argc, char *argv[])
{
    QString selected_tests;

    auto arg_0 = QString(argc > 1 ? argv[1] : "");
    auto arg_1 = QString(argc > 2 ? argv[2] : "");

    if ( arg_0 == "-window" )
        return TestStarter::RunTestsWindow(selected_tests);
    if ( arg_0 == "-console" )
        return TestStarter::RunTestsConsole(selected_tests);
    if ( arg_0 == "-list" )
        return TestStarter::ListAllTests();
    if ( arg_0.contains("-run:") )
        selected_tests = arg_0.replace("-run:", "");
    if ( arg_1 == "-window" )
        return TestStarter::RunTestsWindow(selected_tests);
    if ( arg_1 == "-console" )
        return TestStarter::RunTestsConsole(selected_tests);

    std::cout << "no arguments passed..." << std::endl;
    std::cin.get();
    return 0;
}
