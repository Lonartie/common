#include "ltest_widget.h"
#include "ui_ltest_widget.h"
#include "ltest.h"

#include <QMessageBox>
#include <QString>

ltest_widget::ltest_widget(QWidget *parent) :
    QWidget(parent),
    all_tests(LTest::All_Tests),
    ui(new Ui::ltest_widget)
{
    ui->setupUi(this);
    QMessageBox box(this);
    QString all_names;

    for (auto test : all_tests)
        all_names.push_back(QString("%1/%2\n").arg(test.name_space).arg(test.name));

    box.setText(all_names);
    box.exec();
}

