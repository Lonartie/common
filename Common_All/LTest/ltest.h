#ifndef LTEST_H
#define LTEST_H

#include "ltest_lib.h"
#include "ltest_widget.h"
#include "ltest_starter.h"

#include <map>
#include <vector>

#include <QApplication>

struct LTESTSHARED_EXPORT TestDefinition
{
    const char
        *name,
        *file,
        *line,
        *name_space;
    void(*function)();
};

class LTESTSHARED_EXPORT LTest
{
public:

    static std::vector<TestDefinition> All_Tests;

    static bool AddTest(TestDefinition test);

    static TestDefinition CreateTest(
            const char* name,
            const char* file,
            const char* line,
            const char* space,
            void(*function)());
};

#define __STR(x) #x
#define STRING(x) __STR(x)

#define GROUP_TEST(name_space, test_name)                                           \
void name_space##_##test_name ();                                                   \
static bool name_space##_##test_name##_added = LTest::AddTest(LTest::CreateTest(    \
    #test_name,                                                                     \
    __FILE__,                                                                       \
    STRING(__LINE__),                                                               \
    #name_space,                                                                    \
    & name_space##_##test_name ));                                                  \
void name_space##_##test_name ()

#define TEST(test_name) GROUP_TEST(root, test_name)

#endif // LTEST_H
