#ifndef LTEST_GLOBAL_H
#define LTEST_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(LTEST_LIBRARY)
#  define LTESTSHARED_EXPORT Q_DECL_EXPORT
#else
#  define LTESTSHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // LTEST_GLOBAL_H
