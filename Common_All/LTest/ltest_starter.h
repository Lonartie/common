#ifndef TESTSTARTER_H
#define TESTSTARTER_H

#include "ltest_lib.h"
#include "ltest_widget.h"

#include <QApplication>

class LTESTSHARED_EXPORT TestStarter
{
public:
    static int RunTestsConsole(QString tests);

    static int RunTestsWindow(QString tests);

    static int ListAllTests();

    static int init(int argc, char* argv[]);
};

#define INIT() int main(int argc, char* argv[]) { return TestStarter::init(argc, argv); }

#endif // TESTSTARTER_H
