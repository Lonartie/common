#-------------------------------------------------
#
# Project created by QtCreator 2019-07-04T22:19:43
#
#-------------------------------------------------

QT      += gui widgets core

CONFIG  += console

TARGET = LTest
TEMPLATE = lib

DEFINES += LTEST_LIBRARY

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    ltest.cpp \
    ltest_starter.cpp \
    ltest_widget.cpp

HEADERS += \
        ltest.h \
        ltest_lib.h \
        ltest_starter.h \
        ltest_widget.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}

FORMS += \
    ltest_widget.ui
