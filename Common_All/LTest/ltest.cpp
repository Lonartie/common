#include "ltest.h"
#include <string>

std::vector<TestDefinition> LTest::All_Tests;

bool LTest::AddTest(TestDefinition test)
{
    All_Tests.push_back(test);
    return true;
}

TestDefinition LTest::CreateTest(const char *name, const char *file, const char *line, const char* space, void (*function)())
{
    return TestDefinition {name, file, line, space, function};
}
