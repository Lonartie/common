#ifndef LTEST_WIDGET_H
#define LTEST_WIDGET_H

#include "ltest_lib.h"

#include <QWidget>

#include <memory>

struct TestDefinition;

namespace Ui {
class ltest_widget;
}

class LTESTSHARED_EXPORT ltest_widget : public QWidget
{
    Q_OBJECT

public:

    explicit ltest_widget(QWidget *parent = nullptr);

    ~ltest_widget() = default;

private:

    std::vector<TestDefinition>& all_tests;

    std::shared_ptr<Ui::ltest_widget> ui;
};

#endif // LTEST_WIDGET_H
