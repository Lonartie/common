taskkill /f /im commontests.exe

cd Debug_64
rmdir /s /q output
mkdir output

for /r %%i in (*.dll) do (
 copy "%%i" "output/"
REM windeployqt "output/%%~nxi"
)

for /r %%i in (*.exe) do (
 copy "%%i" "output/"
 windeployqt "output/%%~nxi"
)

cd..
cd Release_64
rmdir /s /q output
mkdir output

for /r %%i in (*.dll) do (
 copy "%%i" "output/"
REM windeployqt "output/%%~nxi"
)

for /r %%i in (*.exe) do (
 copy "%%i" "output/"
 windeployqt "output/%%~nx"
)

set ERRORLEVEL=0